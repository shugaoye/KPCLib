## Change log

### Release 1.2.2
 - Bug fixes

### Release 1.2.1
 - Support Markdown in Notes field
 - Support PassXYZ data format

### Release 1.2.0
 - Added PassXYZLib
 - Support KPCLibPy

### Release 1.1.9
 - Removed dependency of Xamarin.Forms so it can be built with any .Netstandard apps
 - Using SkiaSharp to handle Bitmap which is supported by .Netstandard and .Net Core
 - Removed UWP test app and added .Net Core test app